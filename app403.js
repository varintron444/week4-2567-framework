//Define Object
const user = [
    {
        fname : "Mr.PoPo",
        Address : "15th Park Avenue",
        age: 45,
    },
    {
        fname : "Mr.GG",
        Address : "14th Park Avenue",
        age: 40,
    },
    {
        fname : "Mr.GT",
        Address : "74/8 London",
        age: 35,
    },
];
const drinks =  ["Coke", "Pepsi", "Fanta", "Orange"];
for (const drink in drinks) {
    console.log(drink[0]);
}

const fname = users[0].fname
const fname1 = users[1].fname;
console.log(fname, fname1)

//Object Destructuring with for of loop
//for (const {fname, address, age, salary = 45000} of users) {
for (const user of users) {
    console.log(
        //`Name:${fname} Address: ${address} Age:${age} Salary: ${salary}`
        `Name:${user.fname} Address: ${user.address} Age: ${user.age} Salary: ${user.salary}`
    );
}